﻿using System;
using Coyotes_Food;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Apps apps = new Apps();
            apps.Nachos("beef");
            List<double> list = new List<double>();
            list.Add(apps.Price);
            apps.SWRolls();
            list.Add(apps.Price);
            Order order = new Order();
            order.SetTotalPrice(list);
            Console.WriteLine(apps.FoodToString());
            Console.WriteLine(order.TotalPrice);
            Console.ReadKey();
        }
    }
}
