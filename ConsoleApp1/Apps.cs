﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coyotes_Food
{
    class Apps:ItemProperties
    {
        private ItemProperties food = new ItemProperties();
        public void CupCCQ()
        {
            this.ItemName = ("Cup Fire Roasted Chili Con Queso");
            this.Price = 6.99;
        }
        public void BowlCCQ()
        {
            this.ItemName=("Bowl Fire Roasted Chili Con Queso");
            this.Price = 9.79;
        }
        public void ArtDip()
        {
            this.ItemName = ("Artichoke Dip");
            this.Price = 9.99;
        }
        public void AvoRolls()
        {
            this.ItemName = ("Avocado Rolls");
            this.Price = 9.99;
        }
        public void ChxWings()
        {
            this.ItemName=("Southwest Dry Rubbed Chicken Wings");
            this.Price = 8.99;
        }
        public void CocoShrimp()
        {
            this.ItemName=("Coconut Shrimp");
            this.Price = 8.99;
        }
        public void Nachos(string _MeatOption)
        {
            switch (_MeatOption)
            {
                case ("chicken"):
                    this.ItemName = ("Chicken Coyote Nachos");
                    this.Price = 11.79;
                    break;
                case ("beef"):
                    this.ItemName = ("Beef Coyote Nachos");
                    this.Price = 11.79;
                    break;
                default:
                    this.ItemName = ("Coyote Nachos");
                    this.Price = 9.89;
                    break;
            }
        }
        public void SWRolls()
        {
            this.ItemName = ("SouthWestern Rolls");
            this.Price = 9.49;
        }
       /*
        *   IS IT BETTER TO SITCH USING THE FLAVOR
        *   OR TO OVERRIDE THE PRICE IN THE CASE OF NACHOS
        *       */
        public void AppGuac(string _Flavor)
        {
            switch (_Flavor)
            {
                case ("mango"):
                    this.ItemName = ("Mango Loco Guacamole");
                    this.Price = 9.99;
                    break;
                default:
                    this.ItemName = ("House Guacamole");
                    this.Price = 9.99;
                    break;
            }
        }
        public void BlkBluQues()
        {
            this.ItemName = ("Black And Bleu Quesadilla");
            this.Price = 11.49;
        }
        public void AseQues()
        {
            this.ItemName = ("Asadero Quesadilla");
            this.Price = 11.99;
        }
    }
}
