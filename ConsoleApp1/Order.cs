﻿using System;
using System.Collections.Generic;
using System.Text;
using Coyotes_Food; 

namespace Coyotes_Food
{
    class Order
    {
        private int numberOfItems;
        private double totalPrice;
        private string[] items;

        public int NumberOfItems { get => numberOfItems; set => numberOfItems = value; }
        public double TotalPrice { get => totalPrice; set => totalPrice = value; }
        public string[] Items { get => items; set => items = value; }

        public int SetNumberOfItems(int _Items)
        {
            this.numberOfItems = _Items;
            return numberOfItems;
        }
        public double SetTotalPrice(List<double> _Prices)
        {
            for (int x = 0;x<_Prices.Count;x++)
            {
                this.totalPrice += _Prices[x];
            }
            return totalPrice;
        }
    }
}
