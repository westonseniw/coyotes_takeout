﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Coyotes_Food
{
    class ItemProperties
    {
        private string itemName;
        private double price;
       

        public string ItemName { get => itemName; set => itemName = value; }
        public double Price { get => price; set => price = value; }

       
        public string FoodToString()
        {
            string x = null;
            x += "Item: "+itemName;
            x += "\n";
            x += "Price: "+price.ToString("C2",CultureInfo.CreateSpecificCulture("en-US"));
            return x;
            
        }
    }
}
