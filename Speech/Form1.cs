﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Speech.Synthesis;
using System.Speech.Recognition;
using System.Diagnostics;
using Antlr;
using Newtonsoft;
using WitAi.Models;
using RestSharp;
namespace Speech
{
    public partial class Form1 : Form
    {
        SpeechSynthesizer s = new SpeechSynthesizer();
        bool search = false;
        public Form1()
        {
            InitializeComponent();
            SpeechRecognitionEngine engine = new SpeechRecognitionEngine();
            Choices list = new Choices();
            List<string> words = new List<string>();
            words = LexicalOperator.WordLibrary().ToList();
            s.SelectVoiceByHints(VoiceGender.Female, VoiceAge.Adult);
            list.Add(new string[] { "hello dolores", "open google","help" ,"open notepad","bye","minimize","normal screen","kill this"});

            Grammar grammar = new Grammar(new GrammarBuilder(words.ToString()));

            try
            {
                engine.RequestRecognizerUpdate();
                engine.LoadGrammar(grammar);
                engine.SpeechRecognized += Engine_SpeechRecognized;
                engine.SetInputToDefaultAudioDevice();
                engine.RecognizeAsync(RecognizeMode.Multiple);
            }
            catch { }
        }

        private void Engine_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            string a = e.Result.Text;
           
            switch (a)
            {
                case("hi"):
                    s.SpeakAsync("hi");
                    break;
                case ("open google"):
                    s.SpeakAsync("opening google");
                    Process.Start("https://www.google.com");
                    break;
                case ("open notepad"):
                    s.Speak("opening notepad");
                    Process.Start("notepad.exe");
                    break;
                case("help"):
                    s.SpeakAsync("im not very smart yet. you can say hi open google or open notepad");
                    break;
                case ("bye"):
                    s.Speak("Fuck you, i will kill you");
                    this.Close();
                    break;
                case ("minimize"):
                    WindowState = FormWindowState.Minimized;
                    break;
                case ("normal screen"):
                    WindowState = FormWindowState.Normal;
                    break;
                case ("kill this"):
                    SendKeys.Send("%{f4}");
                    break;

            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
